## Raspi Install Arch Linux

- Install [Arch Linux](https://archlinux.org/) on Raspberry PI 3 or 4
- [Video Tutorial](https://youtu.be/aaPA_UF7oVI)

## Requirements

- SD-Card Reader
- Raspberry Pi 3/4 (tested)
- Windows User (Virtual Machine with Linux)

## Introductions

- step 1 | format sd-card
```bash
gparted -> select sd-card -> unmount all partitions -> remove all partitions
```
- step 2 | create partition (note* change x for your drive)
```bash
lsblk

sudo fdisk /dev/sd(x)

o
n, p, 1, enter, enter, +200M, enter, t, c
n, p, 2, enter, enter, w
```
- step 3 | format partition
```bash
sudo mkfs.vfat /dev/sd(x)1
sudo mkfs.ext4 /dev/sd(x)2
```
- step 4 | create directory
```bash
sudo mkdir /mnt/boot
sudo mkdir /mnt/root
```
- step 5 | mount partition
```bash
sudo mount /dev/sd(x)1 /mnt/boot/
sudo mount /dev/sd(x)2 /mnt/root/
```
- step 6 | [download](https://ca.us.mirror.archlinuxarm.org/os/) archlinux for raspberry pi
```bash
# 32bit:
wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-armv7-latest.tar.gz
```
- step 7 | extract (unzip) .gz file
```bash
sudo su

tar zxvf ArchLinuxARM-rpi-armv7-latest.tar.gz -C /mnt/root
```
- step 8 | open ssh for root (headless)
```bash
nano /mnt/root/etc/ssh/sshd_config

PermitRootLogin yes
```
- step 9 | move files to sd-card
```bash
mv /mnt/root/boot/* /mnt/boot

sync

umount /mnt/root /mnt/boot

eject /dev/sd(x)

rm -r /mnt/boot /mnt/root

exit
```
- step 10 | insert sd-card to raspberry pi
```bash
sd-card -> pi
```
- step 11 | connect via ssh to raspberry pi
```bash
ssh root@your-ip-address-pi
# password: root
```
- step 12 | finish installation
```bash
pacman-key --init
pacman-key --populate archlinuxarm
pacman -Syyu
```
- step 13 | reboot system
```bash
reboot
```
- step 14 | install sudo
```bash
pacman -S sudo

nano /etc/sudoers

%wheel ALL=(ALL) ALL
```
- step 15 | create daily user
```bash
useradd -m new-username -G wheel
passwd new-username
# new password: your-password
# retype new password: your-password
```
- step 16 | change hostname
```bash
echo your-hostname > /etc/hostname
```
- step 17 | change timezone
```bash
ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime
```
- step 18 | reboot system
```bash
reboot
```
- step 19 | login with new username and new password via ssh
```bash
ssh new-username@your-ip-address-pi
```
- step 20 | remove old user and home folder
```bash
sudo userdel alarm

sudo -i
rm -r /home/alarm
exit
```
- step 21 | change default root password 
```bash
sudo -i

passwd
# new password: your-password
# retype new password: your-password
```
- step 22 | disable root for ssh connection
```bash
sudo nano /etc/ssh/sshd_config

PermitRootLogin no
```
- step 23 | reboot system
```bash
sudo reboot
```
- step 24 | (optional* copy sd-card image)
```bash
gparted -> select sd-card -> unmout
sudo dd if=/dev/sd(x) of=myarchlinux.img bs=4M conv=fsync status=progress
```
- step 25 | (optional* activate wifi via ssh)
```bash
# activate wlan0
sudo ifconfig wlan0 up

# iwlist (scan wlan)
sudo iwlist wlan0 scan | grep ESSID
└─> ESSID:"your-essid"

# obfuscate wireless passphrase (optional*)
wpa_passphrase your-essid
└─> your-wifi-password
└─> 64cf3ced850ecef39197bb7b7b301fc39437a6aa6c6a599d0534b16af578e04a

# create file
sudo nano /etc/netctl/mywifi

# paste in file                                          
Description='My Wireless Connection'
Interface=wlan0
Connection=wireless
Security=wpa
IP=dhcp
ESSID='your-essid'
# without obfuscate wireless passphrase
Key='your-password'
# or with obfuscate wireless passphrase
Key=\"64cf3ced850ecef39197bb7b7b301fc39437a6aa6c6a599d0534b16af578e04a

# activate service
sudo netctl enable mywifi
sudo reboot
```